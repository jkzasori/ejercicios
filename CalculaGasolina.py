tkmR=0#total de kilometros recorridos
pgL=0#el precio de la gasolina por litro
dgG=0#el dinero de gasolina gastado en el viaje
ttH=0# el tiempo que se ha tardado en horas
##################
#Calcular:
#Consumo de gasolina(en litros y euros) por cada 100 km
#Consumo de gasolina( en litros y euros) por cada km
#Velocidad medida (en km/h y m/s)
while True:
    try:
        tkmR= float(input("Ingrese el total de Kilómetros recorridos: "))
        if tkmR<0:
            print("Por favor recuerde que el número debe ser positivo\n")
        if tkmR>=0:
            break
    except:
        print("Por favor ingrese un número válido")
while True:
    try:
        pgL= float(input("Ingrese el precio de la gasolina por litro en pesos: "))
        if pgL<0:
            print("Por favor recuerde que el número debe ser positivo\n")
        if pgL>=0:
            break
    except:
        print("Por favor ingrese un número válido")
while True:
    try:
        dgG= float(input("Ingrese el dinero de gasolina gastado en el viaje: "))
        if dgG<0:
            print("Por favor recuerde que el número debe ser positivo\n")
        if dgG>=0:
            break
    except:
        print("Por favor ingrese un número válido")
while True:
    try:
        ttH= float(input("Ingrese el tiempo que se ha tardado en horas: "))
        if ttH<0:
            print("Por favor recuerde que el número debe ser positivo\n")
        if ttH>=0:
            break
    except:
        print("Por favor ingrese un número válido")
TLitros = dgG/pgL #Total de litros gastados
TDinero = dgG/3500 #Dinero gastado en euros
LitrosUnKM= TLitros/tkmR
LitrosCienKM = LitrosUnKM*100
DineroUnKM = TDinero/tkmR
DineroCienKm = DineroUnKM*100
#print("Total litros: ", TLitros)
#print("Total Dinero en euros: ", TDinero)
print("Litros gastados en 1 KM: ",LitrosUnKM)
print("Litros gastados en 100 KM: ", LitrosCienKM)
print("Dinero en Euros gastado en 1 KM: ", DineroUnKM)
print("Dinero en Euros gastado en 100 KM: ", DineroCienKm)
vM=tkmR/ttH
print("Velocidad media en KM/H: ",vM)
vmMS=(1000/3600)*vM
print("Velocidad media en M/S: ",vmMS)